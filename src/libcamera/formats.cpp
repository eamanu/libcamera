/* SPDX-License-Identifier: LGPL-2.1-or-later */
/*
 * Copyright (C) 2019, Google Inc.
 *
 * formats.cpp - Libcamera image formats
 */

#include "formats.h"

/**
 * \file formats.h
 * \brief Types and helper methods to handle libcamera image formats
 */

namespace libcamera {

/**
 * \typedef FormatEnum
 * \brief Type definition for the map of image formats and sizes
 *
 * Type definition used to enumerate the supported pixel formats and image
 * frame sizes. The type associates in a map a pixel format (for memory
 * formats) or a media bus code (for bus formats), to a vector of image
 * resolutions represented by SizeRange items.
 */

} /* namespace libcamera */
